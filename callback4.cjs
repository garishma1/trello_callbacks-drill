let boardsInformation= require("./callback1.cjs");
let listInBoard= require("./callback2.cjs");
let cardsInList= require("./callback3.cjs");


function list(name,array,obj1,obj2){
    try{
    let id=array.reduce((acc,cv)=>{
        if(cv.name===name){
            acc=cv.id;
        }
        return acc;
    },'');

    boardsInformation(id,array,(error,data)=>{
        if(error){
            console.log(error);
        }else{
            console.log(data)
        }

        listInBoard(id,obj1,(error,data)=>{
            if(error){
                console.log(error);
            }else{
                console.log(data);
            }

            let id=data.reduce((acc,cv)=>{
                if(cv.name==="Mind"){
                    acc=cv.id

                }
                return acc;
            },'');

            cardsInList(id,obj2,(error,data)=>{
                if(error){
                    console.log(error);
                }else{
                    console.log(data);
                }
            })
        })
    });
}catch(error){
    console.log(error);
}

};

module.exports=list;


